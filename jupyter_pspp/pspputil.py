#    jupyter-pspp - GNU PSPP kernel for Jupyter
#    Copyright © 2018  Yingtong Li (RunasSudo)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import lxml.etree

import collections

# You thought regex was bad
def parse(output):
	parser = lxml.etree.HTMLParser()
	root = next(lxml.etree.fromstring('<root>' + output + '</root>', parser).iter('root'))
	
	result = collections.OrderedDict()
	for div in root.findall('div'):
		div_name = div.find('h3').text
		result_div = collections.OrderedDict()
		for table in div.findall('table'):
			tbody = table.find('tbody')
			table_name = tbody.find('caption').text
			
			table = Table()
			
			trs = tbody.findall('tr')
			
			tr0 = trs[0]
			# Skip corner - skip tr0[0] as this is never correct
			for j, cell in enumerate(tr0[1:]):
				if 'border-left: double' in cell.get('style'):
					break
			
			colhead_start = j + 1 # +1 to account for skipping first column
			j_span = 0
			
			# Parse column headings
			def parse_column(i, j_span, cell):
				column = Column()
				
				if inner(cell):
					column.name = inner(cell)
				else:
					# Column name is further down
					for tr2 in trs[i + 1:]:
						cur_span = 0
						
						for cell2 in tr2[colhead_start:]:
							if cur_span == j_span and inner(cell2):
								column.name = inner(cell2)
								break
							cur_span += int(cell.get('colspan', 1))
							if cur_span > j_span:
								break
						
						if column.name:
							break
				
				if cell.get('colspan', '1') != '1':
					colspan = int(cell.get('colspan'))
					# Go through children of column
					tr2 = trs[i + 1]
					cur_span = 0
					for cell2 in tr2[colhead_start:]:
						if cur_span >= j_span:
							column.children.append(parse_column(i + 1, cur_span, cell2))
						cur_span += int(cell2.get('colspan', 1))
						if cur_span >= j_span + colspan:
							break
				else:
					table.column_map.append(column)
				
				return column
			
			for cell in tr0[colhead_start:]:
				column = parse_column(0, j_span, cell)
				j_span += int(cell.get('colspan', 1))
				table.columns.append(column)
			
			# Skip corner
			for i, tr in enumerate(trs):
				if 'border-top: thin solid' in tr[0].get('style'):
					break
			
			rowhead_start = i
			
			# Parse row headings and data
			for tr in trs[rowhead_start:]:
				if inner(tr[0]):
					row = Row()
					row.name = inner(tr[0])
					table.rows.append(row)
				else:
					if colhead_start == 1:
						# Untitled subrow
						row = table.rows[-1]
						if len(row.children) == 0:
							# Create a subrow for the parent row
							row2 = Row()
							row2.name = None
							row.children.append(row2)
							table.row_map[-1] = row2 # Update the row map for the parent row
						row2 = Row()
						row2.name = None
						row.children.append(row2)
						row = row2
					else:
						# Part of previous row
						row = table.rows[-1]
				
				if colhead_start == 1:
					table.row_map.append(row)
				
				# Parse subrows
				for j, cell in enumerate(tr[1:colhead_start]):
					if inner(cell):
						row2 = Row()
						row2.name = inner(cell)
						row.children.append(row2)
						row = row2
					else:
						if colhead_start == j + 2:
							# +1 to account for row headers, +1 to get first data column
							# Untitled subrow
							if len(row.children) == 0:
								# Create a subrow for the parent row
								row2 = Row()
								row2.name = None
								row.children.append(row2)
								table.row_map[-1] = row2 # Update the row map for the parent row
							row2 = Row()
							row2.name = None
							row.children.append(row2)
							row = row2
						else:
							# Part of previous subrow
							row = row.children[-1]
					
					if colhead_start == j + 2:
						table.row_map.append(row)
				
				# Parse data
				rowdata = []
				for j, cell in enumerate(tr[colhead_start:]):
					rowdata.append(cell.text)
				table.data.append(rowdata)
			
			result_div[table_name] = table
		
		result[div_name] = result_div
	return result

def inner(xml):
	if xml.text:
		return xml.text + ''.join(lxml.etree.tostring(e, method='text', encoding=str) for e in xml)
	elif len(xml):
		return ''.join(lxml.etree.tostring(e, method='text', encoding=str) for e in xml)
	return None

class Column:
	def __init__(self):
		self.name = None
		self.children = []
	
	def __repr__(self):
		return '<Column {!r}, {} children>'.format(self.name, len(self.children))

class Row:
	def __init__(self):
		self.name = None
		self.children = []
	
	def __repr__(self):
		return '<Row {!r}, {} children>'.format(self.name, len(self.children))

class Table:
	def __init__(self):
		self.columns = []
		self.rows = []
		
		self.column_map = []
		self.row_map = []
		
		self.data = [] # [row][column]
	
	def rowcol_by_name(self, name, l):
		for x in l:
			if x.name == name:
				return x
			if len(x.children) > 0:
				ret = self.rowcol_by_name(name, x.children)
				if ret:
					return ret
		return None
	
	def row_by_name(self, name):
		return self.rowcol_by_name(name, self.rows)
	
	def column_by_name(self, name):
		return self.rowcol_by_name(name, self.columns)
	
	def get_row(self, row):
		if not isinstance(row, Row):
			row = self.row_by_name(row)
		
		if len(row.children) == 0:
			return self.data[self.row_map.index(row)]
		return [self.get_row(x) for x in row.children]
	
	def get_column(self, column):
		if not isinstance(column, Column):
			column = self.column_by_name(column)
		
		if len(column.children) == 0:
			return [row[self.column_map.index(column)] for row in self.data]
		return [self.get_column(x) for x in column.children]
	
	def get_col_in_row(self, column, rowdata):
		if not isinstance(column, Column):
			column = self.column_by_name(column)
		
		if len(column.children) == 0:
			return rowdata[self.column_map.index(column)]
		return [self.get_col_in_row(x, rowdata) for x in column.children]
	
	def get_cell(self, column, row):
		if not isinstance(row, Row):
			row = self.row_by_name(row)
		
		rowdata = self.get_row(row)
		if len(row.children) == 0:
			return self.get_col_in_row(column, rowdata)
		return [self.get_col_in_row(column, subrow) for subrow in rowdata]

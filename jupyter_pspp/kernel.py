#    jupyter-pspp - GNU PSPP kernel for Jupyter
#    Copyright © 2018  Yingtong Li (RunasSudo)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from . import pspputil

import lxml.etree

from pexpect.exceptions import EOF
from pexpect.replwrap import REPLWrapper

from ipykernel.kernelbase import Kernel

import io
import re
import subprocess
import traceback

# Satanic rituals
RE_DIV_OPEN = re.compile(r'<DIV[ >]')
RE_DIV_CLOSE = re.compile(r'</DIV>')
RE_H3 = re.compile(r'<H3>[^<]*</H3>\s*')
RE_ONLY_HEADING = re.compile(r'<DIV[^>]*><H3>[^<]*</H3>\s*</DIV>\s*')

class PSPPKernel(Kernel):
	implementation = 'Jupyter-PSPP'
	implementation_version = '0.1'
	language = 'PSPP'
	language_version = '1.2.0'
	language_info = {
		'name': 'PSPP Syntax',
		'mimetype': 'text/plain',
		'file_extension': '.sps'
	}
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._start_pspp()
		
		# Set up "sandbox" environment
		self.py_globals = {'self': self, 'print': self._print}
		
		self.options = {'suppress_headings': 'empty'}
		
		self.out = None # Most recent PSPP output
		self._out_tables = None # Cache parsed result
	
	def _print(self, *args, **kwargs):
		buf = io.StringIO()
		kwargs['file'] = buf
		print(*args, **kwargs)
		
		stream_content = {'name': 'stdout', 'text': buf.getvalue()}
		self.send_response(self.iopub_socket, 'stream', stream_content)
		buf.close()
	
	def _start_pspp(self):
		# Check the version
		proc = subprocess.Popen(['pspp', '-V'], stdout=subprocess.PIPE, encoding='utf-8')
		self.banner  = '-----------------------------------------------------------------------------\n'
		self.banner += proc.stdout.read()
		self.banner += '-----------------------------------------------------------------------------\n'
		self.banner += 'jupyter-pspp\n'
		self.banner += 'Copyright (C) 2018 Yingtong Li (RunasSudo)\n'
		self.banner += 'This program comes with ABSOLUTELY NO WARRANTY. This program is free software\n'
		self.banner += 'under the terms of the GNU Affero General Public License version 3 or later.\n'
		self.banner += 'See COPYING file or <https://www.gnu.org/licenses/> for details.\n'
		self.banner += '-----------------------------------------------------------------------------'
		
		# Must specify output file before format to get stdout
		self.wrapper = REPLWrapper('pspp -o - -O format=html', 'PSPP>', None, continuation_prompt='    >')
		
		# Initial configuration
		self.wrapper.run_command('SET /JOURNAL=OFF.') # Disable logging to .jnl file
	
	def do_execute(self, code, silent, store_history=True, user_expressions=None, allow_stdin=False):
		try:
			result = self._execute(code, silent)
			
			if result and not silent:
				display_data_content = {'data': {'text/html': result}}
				self.send_response(self.iopub_socket, 'display_data', display_data_content)
			
			return {
				'status': 'ok',
				'execution_count': self.execution_count,
				'payload': [],
				'user_expressions': {},
			}
		except Exception as ex:
			# Gracefully report any errors
			if not silent:
				stream_content = {'name': 'stdout', 'text': traceback.format_exc()}
				self.send_response(self.iopub_socket, 'stream', stream_content)
			
			return {
				'status': 'error',
				'execution_count': self.execution_count,
			}
	
	def _execute(self, code, silent):
		if code.startswith('%'):
			return self._execute_magic(code, silent)
		else:
			return self._execute_pspp(code, silent)
	
	def _execute_pspp(self, code, silent):
		output = self.wrapper.run_command(code)
		
		output = output.strip()
		
		# Associate </DIV> with the correct entry
		if output.startswith('</DIV>'):
			output = output[6:].strip()
		num_divs = len(RE_DIV_OPEN.findall(output))
		if num_divs != len(RE_DIV_CLOSE.findall(output)):
			output += '</DIV>'
		
		if self.options['suppress_headings'] == 'empty' or self.options['suppress_headings'] == 'yes':
			# Strip empty div's
			output = RE_ONLY_HEADING.sub('', output)
		if self.options['suppress_headings'] == 'yes':
			# Remove all headings
			output = RE_H3.sub('', output)
		
		output = output.strip()
		self.out = output
		self._out_tables = None
		return output
	
	def _execute_magic(self, code, silent):
		magic = code.splitlines()[0].split()
		
		if magic[0] == '%set':
			self.options[magic[1]] = magic[2]
			return None
		
		if magic[0] == '%silent':
			psppcode = code[code.index('\n')+1:]
			self._execute_pspp(psppcode, True)
			return None
		
		if magic[0] == '%hide_table':
			psppcode = code[code.index('\n')+1:]
			output = self._execute_pspp(psppcode, True)
			
			tohide = [x.strip() for x in code.splitlines()[0][12:].split(',')]
			root = next(lxml.etree.fromstring('<root>' + output + '</root>', lxml.etree.HTMLParser()).iter('root'))
			for table in root.iter('table'):
				if table.find('tbody').find('caption').text in tohide:
					table.getparent().remove(table)
			
			return lxml.etree.tostring(root, encoding=str).replace('<root>', '').replace('</root>', '')
		
		if magic[0] == '%py':
			pycode = code[code.index('\n')+1:]
			exec(pycode, self.py_globals, self.py_globals)
			return None
	
	def do_is_complete(self, code):
		if code.rstrip().endswith('.'):
			return {
				'status': 'complete',
			}
		else:
			return {
				'status': 'incomplete',
				'indent': '\t',
			}
	
	def do_shutdown(self, *args, **kwargs):
		try:
			self.wrapper.run_command('QUIT.')
		except EOF:
			pass
		
		return super().do_shutdown(*args, **kwargs)
	
	@property
	def out_tables(self):
		if self._out_tables:
			return self._out_tables
		
		result = pspputil.parse(self.out)
		self._out_tables = result
		return result

# jupyter-pspp

A [GNU PSPP](https://www.gnu.org/software/pspp/) kernel for [Jupyter](https://jupyter.org/)

## Repo contents

The *jupyter_pspp* folder is the *jupyter_pspp* module which needs to be installed to Python.

The *pspp* folder contains the [JSON kernel spec file](https://jupyter-client.readthedocs.io/en/latest/kernels.html#kernel-specs) for Jupyter.

## Installation instructions

Install the required dependency [Pexpect](https://pypi.org/project/pexpect/).

Install the *jupyter_pspp* module folder to somewhere accessible by Python.

Then run:

```bash
jupyter kernelspec install /path/to/pspp
```

Where */path/to/pspp* is the path to the *pspp* folder in this repository.

## Usage

You should now be able to create a new Jupyter notebook using the PSPP kernel.

PSPP commands should behave as expected. Tables will be printed in HTML format. Headings in command output will not be printed unless associated with content.

### Configuration

You can configure *jupyter_pspp* by creating and running a cell with the contents:

```
%set [option] [value]
```

* **suppress_headings**: Controls whether headings in PSPP command output will be printed.
  * *empty* (default): Don't print headings unless associated with content.
  * *no*: Print all headings.
  * *yes*: Do not print headings.

Note: Setting options to invalid values results in undefined behaviour.

You can silence the output of a cell by beginning a code block with the line `%silent`.

You can hide certain tables from the output of a cell by beginning a code block with `%hide_table` followed on the same line by a comma-separated list of table captions. For example:

```
%hide_table Summary.,Ranks
```

### Running Python code

You can also run Python code directly by beginning a code block with the line `%py`. `print()` will be automatically overridden to send output to Jupyter:

```py
%py
print('Hello world!')
```

The output from the most recent PSPP execution will be accessible in `self.out`. A parsed version is accessible from `self.out_table`. For example, if we run:

```
CROSSTABS /TABLES=VarX BY VarY /STATISTICS=CORR.
```

We can then run:

```py
%py
print(self.out_tables['CROSSTABS']['Symmetric measures.'].get_cell('Approx. T', 'Spearman Correlation'))
```

To obtain the value of the specified cell.

## Notes

There is no Windows support at present, as Windows does not support pseudo-TTYs, and file redirection puts PSPP into a synchronous processing mode. jupyter-pspp will work fine with PSPP under [Cygwin](https://www.cygwin.com/), but PSPP will need to be compiled from source.
